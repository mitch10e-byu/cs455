//
//  XBoxControllerMapping.h
//  lab3
//
//  Created by Mitchell Tenney on 9/18/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef lab3_XBoxControllerMapping_h
#define lab3_XBoxControllerMapping_h

#include <string>
#include "Logger.h"

// XBOX CONTROLLER MAPPING

#define XBOX_LEFT_STICK_X_AXIS 0
#define XBOX_LEFT_STICK_Y_AXIS 1

#define XBOX_RIGHT_STICK_X_AXIS 2
#define XBOX_RIGHT_STICK_Y_AXIS 3

#define XBOX_LEFT_TRIGGER 4
#define XBOX_RIGHT_TRIGGER 5

#define XBOX_PLAYER_1_LIGHT 0
//#define XBOX_PLAYER_2_LIGHT 1
//#define XBOX_PLAYER_3_LIGHT 2
//#define XBOX_PLAYER_4_LIGHT 3

// Can't figure out what buttons 1-4 on device is...


#define XBOX_DPAD_UP 5
#define XBOX_DPAD_DOWN 6
#define XBOX_DPAD_LEFT 7
#define XBOX_DPAD_RIGHT 8

#define XBOX_START 9
#define XBOX_SELECT 10

#define XBOX_LEFT_STICK_PUSH 11
#define XBOX_RIGHT_STICK_PUSH 12

#define XBOX_LEFT_BUMPER 13
#define XBOX_RIGHT_BUMPER 14
#define XBOX_CENTER_X 15

#define XBOX_A_BUTTON 16
#define XBOX_B_BUTTON 17
#define XBOX_X_BUTTON 18
#define XBOX_Y_BUTTON 19


string getAxisName(int axisID) {
    string result = "UNKNOWN";
    switch (axisID) {
        case XBOX_LEFT_STICK_X_AXIS:
            result = "LEFT STICK X AXIS";
            break;
        case XBOX_LEFT_STICK_Y_AXIS:
            result = "LEFT STICK Y AXIS";
            break;
            
        case XBOX_RIGHT_STICK_X_AXIS:
            result = "RIGHT STICK X AXIS";
            break;
        case XBOX_RIGHT_STICK_Y_AXIS:
            result = "RIGHT STICK Y AXIS";
            break;
            
        case XBOX_LEFT_TRIGGER:
            result = "LEFT TRIGGER";
            break;
        case XBOX_RIGHT_TRIGGER:
            result = "RIGHT TRIGGER";
            break;
            
        default:
            Logger::warn("Unknown Axis!");
            break;
    }
    return result;
}

string getButtonName(int buttonID) {
    string result = "UNKNOWN";
    switch (buttonID) {
        case XBOX_PLAYER_1_LIGHT:
            result = "P1 LIGHT";
            break;
            
        case XBOX_DPAD_UP:
            result = "D-PAD UP";
            break;
        case XBOX_DPAD_DOWN:
            result = "D-PAD DOWN";
            break;
        case XBOX_DPAD_LEFT:
            result = "D-PAD LEFT";
            break;
        case XBOX_DPAD_RIGHT:
            result = "D-PAD RIGHT";
            break;
            
        case XBOX_START:
            result = "START";
            break;
        case XBOX_SELECT:
            result = "SELECT";
            break;
        
        case XBOX_LEFT_STICK_PUSH:
            result = "LEFT STICK";
            break;
        case XBOX_RIGHT_STICK_PUSH:
            result = "RIGHT STICK";
            break;
            
        case XBOX_LEFT_BUMPER:
            result = "LEFT BUMPER";
            break;
        case XBOX_RIGHT_BUMPER:
            result = "RIGHT BUMPER";
            break;
        case XBOX_CENTER_X:
            result = "CENTER X";
            break;
            
        case XBOX_A_BUTTON:
            result = "A";
            break;
        case XBOX_B_BUTTON:
            result = "B";
            break;
        case XBOX_X_BUTTON:
            result = "X";
            break;
        case XBOX_Y_BUTTON:
            result = "Y";
            break;
            
            
        default:
            Logger::warn("Unknown Button!");
            break;
    }
    return result;
}

#endif
