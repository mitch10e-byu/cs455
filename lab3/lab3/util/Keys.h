//
//  Keyboard.h
//  lab3
//
//  Created by Mitchell Tenney on 9/17/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef lab3_Keyboard_h
#define lab3_Keyboard_h

#define KEY_ESCAPE 27
#define KEY_ENTER 13
#define KEY_SPACE 32

#define KEY_A 97
#define KEY_B 98
#define KEY_C 99
#define KEY_D 100
#define KEY_E 101
#define KEY_F 102
#define KEY_G 103
#define KEY_H 104
#define KEY_I 105
#define KEY_J 106
#define KEY_K 107
#define KEY_L 108
#define KEY_M 109
#define KEY_N 110
#define KEY_O 111
#define KEY_P 112
#define KEY_Q 113
#define KEY_R 114
#define KEY_S 115
#define KEY_T 116
#define KEY_U 117
#define KEY_V 118
#define KEY_W 119
#define KEY_X 120
#define KEY_Y 121
#define KEY_Z 122

#endif
