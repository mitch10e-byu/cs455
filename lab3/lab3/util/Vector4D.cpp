//
//  Vector4D.cpp
//  lab3
//
//  Created by Mitchell Tenney on 10/5/15.
//  Copyright © 2015 Mitchell Tenney. All rights reserved.
//

#include "Vector4D.h"

Vector4D::Vector4D() {
    float d[size] = {
        0, 0, 0, 1
    };
    for (int i = 0; i < size; i++) {
        _data[i] = d[i];
    }
}

Vector4D::Vector4D(float *d) {
    for (int i = 0; i < size; i++) {
        _data[i] = d[i];
    }
}

Vector4D::Vector4D(float x, float y, float z, float w) {
    _data[0] = x;
    _data[1] = y;
    _data[2] = z;
    _data[3] = w; // w should always be 1
}

Vector4D::Vector4D(const Vector4D &copy) {
    for (int i = 0; i < size; i++) {
        _data[i] = copy._data[i];
    }
}

float Vector4D::distance(Vector4D &v) {
    float result = 0;
    for (int i = 0; i < size - 1; i++) {
        result += pow((v[i] - _data[i]), 2);
    }
    return result;
}

float Vector4D::length() {
    // sqrt( x^2 + y^2 + z^2)
    float result = 0;
    for (int i = 0; i < size - 1; i++) {
        result += pow(_data[i], 2);
    }
    return sqrt(result);
}

float Vector4D::dot(Vector4D &v) {
    float result = 0;
    for (int i = 0; i < size - 1; i++) {
        result += v[i] * _data[i];
    }
    return result;
}

Vector4D Vector4D::cross(Vector4D &v) {
    Vector4D result;
    result[0] = (_data[1] * v[2]) - (_data[2] * v[1]);
    result[1] = (_data[2] * v[0]) - (_data[0] * v[2]);
    result[2] = (_data[3] * v[1]) - (_data[1] * v[0]);
    result[3] = 1;
    return result;
}



Vector4D Vector4D::normal() {
    Vector4D result = *this;
    result.normalize();
    return result;
}

// Operators

float& Vector4D::operator[](int index) {
    return _data[index];
}

const float& Vector4D::operator[](int index) const{
    return _data[index];
}

Vector4D Vector4D::operator*  (const float scalar) {
    Vector4D result = *this;
    result *= scalar;
    return result;
}

Vector4D Vector4D::operator/  (const float scalar) {
    Vector4D result = *this;
    result /= scalar;
    return result;
}

Vector4D Vector4D::operator+  (Vector4D &v){
    Vector4D result = *this;
    result += v;
    return result;
}


Vector4D Vector4D::operator-  (Vector4D &v){
    Vector4D result = *this;
    result -= v;
    return result;
}

Vector4D Vector4D::operator*  (Vector4D &v){
    Vector4D result = *this;
    result *= v;
    return result;
}

Vector4D Vector4D::operator/  (Vector4D &v){
    Vector4D result = *this;
    result /= v;
    return result;
}

Vector4D& Vector4D::operator=(const Vector4D &v) {
    for (int i = 0; i < size; i++) {
        _data[i] = v[i];
    }
    return *this;
}

Vector4D& Vector4D::operator*= (const float scalar) {
    for (int i = 0; i < size; i++) {
        _data[i] *= scalar;
    }
    return *this;
}

Vector4D& Vector4D::operator/= (const float scalar) {
    for (int i = 0; i < size; i++) {
        _data[i] /= scalar;
    }
    return *this;
}

Vector4D& Vector4D::operator+= (const Vector4D &v){
    for (int i = 0; i < size; i++) {
        _data[i] += v[i];
    }
    return *this;
}

Vector4D& Vector4D::operator-= (const Vector4D &v){
    for (int i = 0; i < size; i++) {
        _data[i] -= v[i];
    }
    return *this;
}

Vector4D& Vector4D::operator*= (const Vector4D &v){
    for (int i = 0; i < size; i++) {
        _data[i] *= v[i];
    }
    return *this;
}

Vector4D& Vector4D::operator/= (const Vector4D &v){
    for (int i = 0; i < size; i++) {
        _data[i] /= v[i];
    }
    return *this;
}

