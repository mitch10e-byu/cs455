//
//  Controller.h
//  lab3
//
//  Created by Mitchell Tenney on 9/17/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//
//  Using Libstem_Gamepad library, as GLUT doesn't get the right controller stick or the triggers on my XBOX Controller.
//  Thanks to: https://github.com/ThemsAllTook/libstem_gamepad
//      http://forums.tigsource.com/index.php?topic=10675.0
//  Works great as long as you have a driver to read in xbox controller on the mac already: If you can play a game with your controller, this lib will read that controller

#ifndef lab3_Controller_h
#define lab3_Controller_h
#pragma once

#include <sstream>
#include "Logger.h"
#include "Gamepad.h"
#include <cmath>
#include <math.h>
#include "XBoxControllerMapping.h"

#define POLL_ITERATION_INTERVAL 30
int buttons[20] = {0};
float axes[6] = {0.0f};

void pollController() {
    int iterationsToNextPoll = POLL_ITERATION_INTERVAL;
    iterationsToNextPoll--;
    if (iterationsToNextPoll == 0) {
        Gamepad_detectDevices();
        iterationsToNextPoll = POLL_ITERATION_INTERVAL;
    }
    Gamepad_processEvents();
}

void onButtonDown(struct Gamepad_device * device, unsigned int buttonID, double timestamp, void * context) {
    stringstream ss;
    ss << "Device " << device->deviceID << " pushed:   " << getButtonName(buttonID);
    Logger::debug(ss.str());
    if(buttons[buttonID] != -1) {
        buttons[buttonID] = 1;
    }
}

void onButtonUp(struct Gamepad_device * device, unsigned int buttonID, double timestamp, void * context) {
    stringstream ss;
    ss << "Device " << device->deviceID << " released: " << getButtonName(buttonID);
    Logger::debug(ss.str());
    buttons[buttonID] = 0;
}

void onAxisMoved(struct Gamepad_device * device, unsigned int axisID, float value, float lastValue, double timestamp, void * context) {
    float diff = abs(lastValue - value);
    if(diff > 0.01) {
        stringstream ss;
        ss << "Device " << device->deviceID << " moved axis: " << getAxisName(axisID) << " " << axisID <<"  to " << roundf(value * 10) / 10;
        Logger::debug(ss.str());
        axes[axisID] = roundf(value * 10) / 10;
    }
}

void onDeviceAttached(struct Gamepad_device *device, void *context) {
    stringstream ss;
    ss << "Device " << device->deviceID << " attached: " << device->description;
    Logger::info(ss.str());
}

void onDeviceRemoved(struct Gamepad_device *device, void *context) {
    stringstream ss;
    ss << "Device ID " << device->deviceID << " removed: " << device->description;
    Logger::info(ss.str());
}

void initGamepad() {
    Logger::debug("Loading Controllers");
    Gamepad_deviceAttachFunc(onDeviceAttached, (void *) 0x1);
    Gamepad_deviceRemoveFunc(onDeviceRemoved, (void *) 0x2);
    Gamepad_buttonDownFunc(onButtonDown, (void *) 0x3);
    Gamepad_buttonUpFunc(onButtonUp, (void *) 0x4);
    Gamepad_axisMoveFunc(onAxisMoved, (void *) 0x5);
    Gamepad_init();
}






#endif
