//
//  Matrix.h
//  lab3
//
//  Created by Mitchell Tenney on 10/5/15.
//  Copyright © 2015 Mitchell Tenney. All rights reserved.
//

#ifndef Matrix_h
#define Matrix_h
#pragma once

#include <stdio.h>
#include <math.h>
#include <string>
#include "Vector4D.h"

#define PI M_PI

using namespace std;

class Matrix {
public:
    Matrix();
    Matrix(float *d);
    Matrix(const Matrix &copy);
    
    static Matrix Identity();
    static Matrix Rotate(char axis, float angle);
    static Matrix RotateAll(float x, float y, float z);
    static Matrix Translate(float x, float y, float z);
    static Matrix Scale(float x, float y, float z);
    
    float at(int row, int col);
    float* matrix();
    string info();
    
    // Operators
    float       &operator[] (int index);
    const float &operator[] (int index) const;
    
    Vector4D    operator* (Vector4D &v);
    
    Matrix  operator*  (const float scalar);
    Matrix  operator+  (Matrix &m);
    Matrix  operator*  (Matrix &m);
    
    Matrix  &operator*= (const float scalar);
    Matrix  &operator=  (const Matrix &m);
    Matrix  &operator+= (const Matrix &m);
    Matrix  &operator*= (const Matrix &m);
    
    static float degreesToRadians(float degrees) {
        return (degrees * PI / 180);
    }
    
    
private:
    static Matrix RotateX(float angle) {
//        angle = degreesToRadians(angle);
        float cAngle = cos(angle);
        float sAngle = sin(angle);
        float snAngle = -sin(angle);
        float d[16] = {
            1, 0,       0,          0,
            0, cAngle,  sAngle,     0,
            0, snAngle, cAngle,     0,
            0, 0,       0,          1
        };
        return Matrix(d);
    }
    
    static Matrix RotateY(float angle) {
//        angle = degreesToRadians(angle);
        float cAngle = cos(angle);
        float sAngle = sin(angle);
        float snAngle = -sin(angle);
        float d[16] = {
            cAngle,     0, snAngle, 0,
            0,          1, 0,       0,
            sAngle,     0, cAngle,  0,
            0,          0, 0,       1
        };
        return Matrix(d);
    }
    
    static Matrix RotateZ(float angle) {
//        angle = degreesToRadians(angle);
        float cAngle = cos(angle);
        float sAngle = sin(angle);
        float snAngle = -sin(angle);
        float d[16] = {
            cAngle,     sAngle, 0, 0,
            snAngle,    cAngle, 0, 0,
            0,          0,      1, 0,
            0,          0,      0, 1
        };
        return Matrix(d);
    }
    
    static const int size = 16;
    float _data[size];
};

#endif /* Matrix_h */
