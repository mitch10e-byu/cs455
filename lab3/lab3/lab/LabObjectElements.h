//
//  LabObjectElements.h
//  lab3
//
//  Created by Mitchell Tenney on 9/17/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef lab3_LabObjectElements_h
#define lab3_LabObjectElements_h
#pragma once

#include <vector>
#include <string>
#include "Matrix.h"
using namespace std;

typedef struct {
    float x;
    float y;
} vector2D;

typedef struct {
    float x;
    float y;
    float z;
} vector3D;

typedef struct {
    float x;
    float y;
    float z;
    float w;
} vector4D;

typedef struct {
    vector3D ambient;
    vector3D diffuse;
    vector3D specular;
    vector3D transmittance;
    vector3D emission;
    float shininess;
    float ior;
    float dissolve;
    int illumination;
    
    string ambient_texture;         // map_Ka
    string diffuse_texture;         // map_Kd
    string specular_texture;        // map_Ks
    string specular_highlight;      // map_Ns
    string bump_texture;            // map_bump
    string displacement_texture;    // disp
    string alpha_texture;           // map_d
    
} material_t;

typedef struct {
    int vertex;
    int texture;
    int normal;
} face_value;

typedef struct {
    vector<face_value> values;
} face_t;

typedef struct {
    vector<vector3D> vertex;
    vector<vector2D> uv;
    vector<vector3D> normal;
    vector<face_t> faces;
    int materialID;
} mesh_t;

typedef struct {
    string name;
    mesh_t mesh;
} shape;

#endif
