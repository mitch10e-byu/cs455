//
//  Lab.h
//  lab3
//
//  Created by Mitchell Tenney on 9/17/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef __lab3__Lab__
#define __lab3__Lab__
#pragma once

//Only for windows
#ifdef WIN32
#include <Windows.h>
#include <MMSystem.h>
#endif // WIN32

// For all operating systems
#include <OpenGL/GL.h>
#include <OpenGL/GLU.h>
#include <GLUT/glut.h>
#include "SOIL.h"


#include <iostream>
#include <vector>
#include <math.h>

#include "Matrix.h"
#include "Keys.h"
#include "Controller.h"
#include "LabObject.h"
#include "LabObjectLoader.hpp"
#include "LabControls.h"
#include "LabCamera.h"
#include "Logger.h"

#define PI M_PI

#define DEFAULT_WIDTH   800
#define DEFAULT_HEIGHT  450

int width = DEFAULT_WIDTH;
int height = DEFAULT_HEIGHT;
bool stereo = false;
bool isFullscreen = false;
bool keys[300] = {0};
bool specialKeys[200] = {0};
string objectNames[7] = {"car", "tire", "tire", "tire", "tire", "parkingLot", "molecart"};
vector<LabObject> objects;
vector<LabObject *> inScene;
GLuint textures[6];
LabCamera* camera;

#define NORMAL_EXIT_GLUT_LOOP "Exiting"

void initGLUT(int argc, char **argv);
void initGL();
void idle();
void resize(int w, int h);
void render();
void pollController();
void handleControllerInput();
void handleKeyboardInput();
void keyboard(unsigned char key, int x, int y);
void keyboardup(unsigned char key, int x, int y);
void specialKey(int key, int x, int y);
void specialKeyUp(int key, int x, int y);
void loadObjects();
void loadTextures();
void openWindow();
bool controllerInUse();
void loadScene();
void turnTiresLeft();
void turnTiresRight();
void toggleFullscreen();

void run(int argc, char **argv) {
    camera = LabCamera::get();
    initGLUT(argc, argv);
    initGL();
    loadObjects();
    loadTextures();
    loadScene();
    initGamepad();
    openWindow();
}

void idle() {
    pollController();
    handleControllerInput();
    handleKeyboardInput();
    render();
}

void loadScene() {
    
    LabObject *parkingLot = &objects[5];
    parkingLot->rotatef(0, - PI / 3, 0);
    parkingLot->scalef(2, 2, 2);
    
    LabObject *car = &objects[0];
    car->setParent(parkingLot);
    car->rotatef(0, PI / 3, 0);
    car->scalef(1.5, 1.5, 1.5);
    car->translatef(-2, 0, -7.25);
    
    LabObject *wheel_fl = &objects[1];
    wheel_fl->setParent(car);
    wheel_fl->setID("tireFL");
    wheel_fl->rotatef(0, PI, 0);
    wheel_fl->scalef(0.25, 0.25, 0.25);
    wheel_fl->translatef(-0.4, 0.15, -0.55);
    
    LabObject *wheel_fr = &objects[2];
    wheel_fr->setParent(car);
    wheel_fr->setID("tireFR");
    wheel_fr->scalef(0.25, 0.25, 0.25);
    wheel_fr->translatef(0.4, 0.15, -0.55);
    
    LabObject *wheel_bl = &objects[3];
    wheel_bl->setParent(car);
    wheel_bl->setID("tireBL");
    wheel_bl->rotatef(0, PI, 0);
    wheel_bl->scalef(0.25, 0.25, 0.25);
    wheel_bl->translatef(-0.4, 0.15, 0.475);
    
    LabObject *wheel_br = &objects[4];
    wheel_br->setParent(car);
    wheel_br->setID("tireBR");
    wheel_br->scalef(0.25, 0.25, 0.25);
    wheel_br->translatef(0.4, 0.15, 0.475);
    
    LabObject *mole = &objects[6];
    mole->setParent(car);
    mole->setID("mole");
    mole->scalef(0.05, 0.05, 0.05);
    mole->rotatef(0, PI / 2, 0);
    mole->translatef(0, 0.75, 0);
    
    inScene.push_back(parkingLot);
    inScene.push_back(car);
    inScene.push_back(wheel_fl);
    inScene.push_back(wheel_fr);
    inScene.push_back(wheel_bl);
    inScene.push_back(wheel_br);
    inScene.push_back(mole);
    
    if(Logger::enableDebug) {
        stringstream ss;
        ss << "Objects in Scene: " << (inScene.size());
        Logger::debug(ss.str());
        for (int i = 0; i < (inScene.size()); i++) {
            Logger::debug(inScene[i]->getID());
        }
    }
}

void keyboard(unsigned char key, int x, int y) {
    if(key == KEY_ESCAPE) {
        throw NORMAL_EXIT_GLUT_LOOP;
    }
    if(!controllerInUse()) {
        if(key == KEY_P && (keys[KEY_P] == 0)) {
            keys[KEY_P] = -1;
            toggleFullscreen();
        }
        if(key == KEY_W) {
            keys[KEY_W] = 1;
        }
        if(key == KEY_A) {
            keys[KEY_A] = 1;
        }
        if(key == KEY_S) {
            keys[KEY_S] = 1;
        }
        if(key == KEY_D) {
            keys[KEY_D] = 1;
        }
        
        if(key == KEY_Q) {
            keys[KEY_Q] = 1;
        }
        if(key == KEY_E) {
            keys[KEY_E] = 1;
        }
        
        if(key == KEY_R) {
            keys[KEY_R] = 1;
        }
        if(key == KEY_F) {
            keys[KEY_F] = 1;
        }
    }
}

void keyboardup(unsigned char key, int x, int y) {
    if(key == KEY_W) {
        keys[KEY_W] = 0;
    }
    if(key == KEY_A) {
        keys[KEY_A] = 0;
    }
    if(key == KEY_S) {
        keys[KEY_S] = 0;
    }
    if(key == KEY_D) {
        keys[KEY_D] = 0;
    }
    
    if(key == KEY_Q) {
        keys[KEY_Q] = 0;
    }
    if(key == KEY_E) {
        keys[KEY_E] = 0;
    }
    
    if(key == KEY_R) {
        keys[KEY_R] = 0;
    }
    if(key == KEY_F) {
        keys[KEY_F] = 0;
    }
    
    if(key == KEY_P) {
        keys[KEY_P] = 0;
    }

}

void specialKey(int key, int x, int y) {
    switch(key){
        case GLUT_KEY_LEFT:
            specialKeys[GLUT_KEY_LEFT] = 1;
            break;
        case GLUT_KEY_RIGHT:
            specialKeys[GLUT_KEY_RIGHT] = 1;
            break;
        case GLUT_KEY_DOWN:
            specialKeys[GLUT_KEY_DOWN] = 1;
            break;
        case GLUT_KEY_UP:
            specialKeys[GLUT_KEY_UP] = 1;
            break;
        default:
            break;
    }
}

void specialKeyUp(int key, int x, int y) {
    switch(key){
        case GLUT_KEY_LEFT:
            specialKeys[GLUT_KEY_LEFT] = 0;
            break;
        case GLUT_KEY_RIGHT:
            specialKeys[GLUT_KEY_RIGHT] = 0;
            break;
        case GLUT_KEY_DOWN:
            specialKeys[GLUT_KEY_DOWN] = 0;
            break;
        case GLUT_KEY_UP:
            specialKeys[GLUT_KEY_UP] = 0;
            break;
        default:
            break;
    }
}

void handleKeyboardInput() {
    if(keys[KEY_W] == 1) {
        camera->moveForward(-1);
    }
    if(keys[KEY_A] == 1) {
        camera->strafeLeft(-1);
    }
    if(keys[KEY_S] == 1) {
        camera->moveBackward(1);
    }
    if(keys[KEY_D]) {
        camera->strafeRight(1);
    }
    
    if(keys[KEY_Q] == 1) {
        camera->turnLeft(-1);
    }
    if(keys[KEY_E] == 1) {
        camera->turnRight(1);
    }
    
    if(keys[KEY_R] == 1) {
        camera->lookUp(1);
    }
    if(keys[KEY_F] == 1) {
        camera->lookDown(-1);
    }
    
    if(specialKeys[GLUT_KEY_UP] == 1){
        camera->moveUp();
    }
    if(specialKeys[GLUT_KEY_DOWN] == 1){
        camera->moveDown();
    }
    
    if(specialKeys[GLUT_KEY_LEFT] == 1){
        turnTiresLeft();
    }
    if(specialKeys[GLUT_KEY_RIGHT] == 1){
        turnTiresRight();
    }
    
}



void turnTiresLeft() {
    Logger::debug("Turn Tire Left");
    float rotation = 0.02;
    for(int i = 0; i < (inScene.size()); i++) {
        if(inScene[i]->getID() == "tireFL" || inScene[i]->getID() == "tireFR") {
            if(inScene[i]->yRotation < (PI / 4)) {
                inScene[i]->rotatef(0, rotation, 0);
                inScene[i]->yRotation += rotation;
            } else {
                inScene[i]->yRotation = PI / 4;
            }
        }
    }
}

void turnTiresRight() {
    Logger::debug("Turn Tire Right");
    float rotation = 0.02;
    for(int i = 0; i < (inScene.size()); i++) {
        if(inScene[i]->getID() == "tireFL" || inScene[i]->getID() == "tireFR") {
            if(inScene[i]->yRotation > (-PI / 4)) {
                inScene[i]->rotatef(0, -rotation, 0);
                inScene[i]->yRotation -= rotation;
            } else {
                inScene[i]->yRotation = (-PI / 4);
            }
        }
    }
}

void handleControllerInput() {
    float axisDetection = 0.2;
    if(buttons[XBOX_CENTER_X] == 1) {
        throw NORMAL_EXIT_GLUT_LOOP;
    }
    if(buttons[XBOX_SELECT] == 1) {
        buttons[XBOX_SELECT] = -1;
        toggleFullscreen();
    }
    
    if(buttons[XBOX_START] == 1) {
        camera->home();
    }
    
    if(buttons[XBOX_DPAD_DOWN] == 1) {
        camera->moveDown();
    }
    if(buttons[XBOX_DPAD_UP] == 1) {
        camera->moveUp();
    }
    
    if(buttons[XBOX_DPAD_LEFT] == 1) {
        turnTiresLeft();
    }
    if(buttons[XBOX_DPAD_RIGHT] == 1) {
        turnTiresRight();
    }
    
    if(axes[XBOX_RIGHT_STICK_Y_AXIS] < -axisDetection) {
        camera->lookDown(axes[XBOX_RIGHT_STICK_Y_AXIS]);
    }
    if(axes[XBOX_RIGHT_STICK_Y_AXIS] > axisDetection) {
        camera->lookUp(axes[XBOX_RIGHT_STICK_Y_AXIS]);
    }
    
    if(axes[XBOX_RIGHT_STICK_X_AXIS] < -axisDetection) {
        camera->turnRight(axes[XBOX_RIGHT_STICK_X_AXIS]);
    }
    if(axes[XBOX_RIGHT_STICK_X_AXIS] > axisDetection) {
        camera->turnLeft(axes[XBOX_RIGHT_STICK_X_AXIS]);
    }
    
    if(axes[XBOX_LEFT_STICK_Y_AXIS] > axisDetection) {
        camera->moveForward(axes[XBOX_LEFT_STICK_Y_AXIS]);
    }
    if(axes[XBOX_LEFT_STICK_Y_AXIS] < -axisDetection) {
        camera->moveBackward(axes[XBOX_LEFT_STICK_Y_AXIS]);
    }
    
    if(axes[XBOX_LEFT_STICK_X_AXIS] > axisDetection) {
        camera->strafeLeft(axes[XBOX_LEFT_STICK_X_AXIS]);
    }
    if(axes[XBOX_LEFT_STICK_X_AXIS] < -axisDetection) {
        camera->strafeRight(axes[XBOX_LEFT_STICK_X_AXIS]);
    }
    
}

void render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, width, height);
    glRotatef(- PI / 3, 0, 1, 0);
    glRotatef(camera->rotation().x, 1, 0, 0);
    glRotatef(camera->rotation().y, 0, 1, 0);
    glRotatef(camera->rotation().z, 0, 0, 1);
    
    glTranslatef(camera->position().x, camera->position().y, camera->position().z);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    for(int i = 0; i < inScene.size(); i++) {
        GLuint material = textures[inScene[i]->getMesh().materialID];
        glBindTexture(GL_TEXTURE_2D, material);
        inScene[i]->transform();
        inScene[i]->draw();
    }
    glFlush();
    glutSwapBuffers();
}

void loadObjects() {
    Logger::info("Loading Objects");
    LabObjectLoader loader = LabObjectLoader();
    for(int i = 0; i < ARRAY_SIZE(objectNames); i++) {
        LabObject obj = loader.loadObject(objectNames[i]);
        objects.push_back(obj);
    }
}

void loadTextures() {
    Logger::info("Loading Textures");
    for(int t = 0; t < 6; t++) {
        switch(t) {
            case 0:
                textures[t] =
                SOIL_load_OGL_texture("resource/car.bmp",
                                      SOIL_LOAD_AUTO,
                                      SOIL_CREATE_NEW_ID,
                                      SOIL_FLAG_INVERT_Y
                                      );
                break;
            case 1:
            case 2:
            case 3:
            case 4:
                textures[t] =
                SOIL_load_OGL_texture("resource/tire.bmp",
                                      SOIL_LOAD_AUTO,
                                      SOIL_CREATE_NEW_ID,
                                      SOIL_FLAG_INVERT_Y
                                      );
                break;
            case 5:
                textures[t] =
                SOIL_load_OGL_texture("resource/parkingLot.bmp",
                                      SOIL_LOAD_AUTO,
                                      SOIL_CREATE_NEW_ID,
                                      SOIL_FLAG_INVERT_Y
                                      );
                break;
        }
        if(textures[t] == 0) {
            Logger::error("Unable to load texture: " + objectNames[t]);
        }
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        mesh_t m = objects[t].getMesh();
        m.materialID = t;
        objects[t].setMesh(m);
    }
}

void initGLUT(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutInitWindowPosition((glutGet(GLUT_SCREEN_WIDTH) - width) / 2,
                           (glutGet(GLUT_SCREEN_HEIGHT) - height) / 2);
    glutCreateWindow("Lab 3");
    
    glutDisplayFunc(render);
    glutIdleFunc(idle);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyboardup);
    glutSpecialFunc(specialKey);
    glutSpecialUpFunc(specialKeyUp);
    
    
}

void initGL() {
    glShadeModel(GL_SMOOTH);
    glClearColor(0.5f, 0.5f, 0.5f, 0.5f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_COLOR_MATERIAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void resize(int w, int h) {
    if(h == 0)
        h = 1;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(stereo)
        gluPerspective(45.0f, (GLfloat) width / ((GLfloat) height * 2.0f), 0.1f, 2000.0f);
    else
        gluPerspective(45.0f, (GLfloat) width / (GLfloat) height, 0.1f, 2000.0f);
    width = w;
    height = h;
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

bool controllerInUse() {
    bool controllerPluggedIn = false;
    if(Gamepad_numDevices() > 0) {
        controllerPluggedIn = true;
    }
    return controllerPluggedIn;
}

void toggleFullscreen() {
    isFullscreen = !isFullscreen;
    if(isFullscreen) {
        fullscreen();
    } else {
        width = DEFAULT_WIDTH;
        height = DEFAULT_HEIGHT;
        window(width, height);
    }
}

void openWindow() {
    Logger::info("Running Lab");
    try {
        glutMainLoop();
    } catch (const char* msg) {
        Logger::info(msg);
    }
}

#endif /* defined(__lab3__Lab__) */
