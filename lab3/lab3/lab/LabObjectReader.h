//
//  LabObjectReader.h
//  lab3
//
//  Created by Mitchell Tenney on 9/17/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef lab3_LabObjectReader_h
#define lab3_LabObjectReader_h
#pragma once


#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "LabObjectElements.h"
#include "Logger.h"

using namespace std;

class LabObjectReader {
public:
    LabObjectReader();
    ~LabObjectReader();
    bool load(string obj, string tex); // .obj file, .bmp file
    
    
    
private:
    
    vector<GeometricVertex> vertices;
    vector<TextureVertex> textures;
    vector<NormalVertex> normals;
    vector<Face> faces;
    
    void createVertex(vector<string> &tokens) {
        GeometricVertex v = GeometricVertex();
        v.x = stof(tokens[1]);
        v.y = stof(tokens[2]);
        v.z = stof(tokens[3]);
        vertices.push_back(v);
    }
    
    void createVertexTexture(vector<string> &tokens) {
        TextureVertex vt = TextureVertex();
        vt.u = stof(tokens[1]);
        vt.v = stof(tokens[2]);
        textures.push_back(vt);
    }
    
    void createVertexNormal(vector<string> &tokens) {
        NormalVertex vn = NormalVertex();
        vn.i = stof(tokens[1]);
        vn.j = stof(tokens[2]);
        vn.k = stof(tokens[3]);
        normals.push_back(vn);
    }
    
    void createFace(vector<string> &tokens) {
        Face face = Face();
        for(int i = 1; i < tokens.size(); i++) {
            vector<string> tokenValues = splitFace(tokens[i]);
            FaceValue faceValue = FaceValue();
            faceValue.v = stoi(tokenValues[0]);
            faceValue.vt = stoi(tokenValues[1]);
            faceValue.vn = stoi(tokenValues[2]);
            face.vals.push_back(faceValue);
        }
        faces.push_back(face);
        
    }
    
    vector<string> splitFace(string &token) {
        vector<string> tokenValues = vector<string>();
        stringstream ss(token);
        string item;
        while(getline(ss, item, '/')) {
            tokenValues.push_back(item);
        }
        return tokenValues;
    }
    
    vector<string> getTokens(string &line) {
        vector<string> tokens;
        stringstream ss(line);
        string token;
        while(ss >> token) {
            tokens.push_back(token);
        }
        return tokens;
    }
    
};



#endif
