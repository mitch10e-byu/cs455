//
//  LabObject.h
//  lab3
//
//  Created by Mitchell Tenney on 9/17/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef __lab3__LabObject__
#define __lab3__LabObject__
#pragma once

#include <stdio.h>
#include <vector>
#include "LabObjectElements.h"
#include "Logger.h"

class LabObject {
public:
    
    LabObject();
    
    string  getID();
    void    setID(string ID);
    
    mesh_t  getMesh();
    void    setMesh(mesh_t mesh);
    
    LabObject* getParent();
    void    setParent(LabObject *parent);
    
    void    rotatev(vector3D &v);
    void    rotatef(float x, float y, float z);
    void    scalev(vector3D &v);
    void    scalef(float x, float y, float z);
    void    translatev(vector3D &v);
    void    translatef(float x, float y, float z);
    
    void    transform();
    void    draw();
    void    getInfo();
    
    float   yRotation;
    
private:
    string _id;
    mesh_t _mesh;
    Matrix _rotation;
    Matrix _scale;
    Matrix _translation;
    LabObject *_parent;
};

#endif /* defined(__lab3__LabObject__) */
