//
//  LabCamera.cpp
//  lab3
//
//  Created by Mitchell Tenney on 10/5/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#include "LabCamera.h"
#include <math.h>

LabCamera* LabCamera::_instance = NULL;

float radians(float degrees) {
    return (degrees * M_PI / 180);
}

LabCamera::LabCamera() {
    home();
}

LabCamera* LabCamera::get() {
    if(_instance == NULL) {
        _instance = new LabCamera();
    }
    return _instance;
}

void LabCamera::home() {
    _position.x = _home[0];
    _position.y = _home[1];
    _position.z = _home[2];
    _rotation.x = 0;
    _rotation.y = 0;
    _rotation.z = 0;
}

vector3D LabCamera::position() {
    return _position;
}

vector3D LabCamera::rotation() {
    return _rotation;
}

void LabCamera::moveForward(float speed) {
    _position.x += _speed * speed * sin(radians(_rotation.y));
    _position.z -= _speed * speed * cos(radians(_rotation.y));
}

void LabCamera::moveBackward(float speed) {
    _position.x += _speed * speed * sin(radians(_rotation.y));
    _position.z -= _speed * speed * cos(radians(_rotation.y));
}

void LabCamera::strafeLeft(float speed) {
    _position.x -= _speed * speed * sin(radians(_rotation.y + 90));
    _position.z += _speed * speed * cos(radians(_rotation.y + 90));
}

void LabCamera::strafeRight(float speed) {
    _position.x += _speed * speed * sin(radians(_rotation.y - 90));
    _position.z -= _speed * speed * cos(radians(_rotation.y - 90));
}

void LabCamera::moveUp() {
    _position.y -= _speed;
    if(_position.y <= -5) {
        _position.y = -5;
    }
}

void LabCamera::moveDown() {
    _position.y += _speed;
    if(_position.y >= -0.25) {
        _position.y = -0.25;
    }
}

void LabCamera::turnLeft(float speed) {
    _rotation.y += _speed * speed * _rotationSpeed;
}

void LabCamera::turnRight(float speed) {
    _rotation.y += _speed * speed * _rotationSpeed;
}

void LabCamera::lookDown(float speed) {
    _rotation.x -= _speed * speed * _rotationSpeed;
    if(_rotation.x >= 90) {
        _rotation.x = 90;
    }
}

void LabCamera::lookUp(float speed) {
    _rotation.x -= _speed * speed * _rotationSpeed;
    if(_rotation.x <= -90) {
        _rotation.x = -90;
    }
}