//
//  LabObjectLoader.cpp
//  lab3
//
//  Created by Mitchell Tenney on 10/2/15.
//  Copyright © 2015 Mitchell Tenney. All rights reserved.
//

#include "LabObjectLoader.hpp"
#include "Logger.h"
#include <fstream>
#include <sstream>

LabObject LabObjectLoader::loadObject(string objectName) {
    LabObject object = LabObject();
    object.setID(objectName);
    vector<vector3D> vertex;
    vector<vector2D> textures;
    vector<vector3D> normals;
    vector<face_t> faces;
    
    ifstream file;
    file.open(resourceFolder + objectName + ".obj");
    if(file.is_open()) {
        Logger::debug("Successfully opened " + objectName);
        string line;
        while(!file.eof()) {
            getline(file, line);
            stringstream ss(line);
            vector<string> tokens;
            string token;
            while(ss >> token) {
                tokens.push_back(token);
            }
            if(!tokens.empty()) {
                if(tokens[0] == "v") {
                    vertex.push_back(createVector3D(tokens));
                } else if(tokens[0] == "vt") {
                    textures.push_back(createVector2D(tokens));
                } else if(tokens[0] == "vn") {
                    normals.push_back(createVector3D(tokens));
                } else if(tokens[0] == "f") {
                    faces.push_back(createFace(tokens));
                }
            }
        }
        file.close();
        if(!file.is_open()) {
            Logger::debug(objectName + " is closed");
        }
    } else {
        Logger::error(objectName + " did not open.");
    }
    
    mesh_t m = object.getMesh();
    m.vertex = vertex;
    m.uv = textures;
    m.normal = normals;
    m.faces = faces;
    object.setMesh(m);
    return object;
}