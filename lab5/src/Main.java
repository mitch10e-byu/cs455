import tracer.Tracer;

public class Main {

    public static void main(String[] args) {
        System.out.println("Ray Tracer");
        runTracer("test1.scene");
        runTracer("test2.scene");
        runTracer("test3.scene");
        runTracer("test4.scene");
        runTracer("test5.scene");
//        makeGIFImages();
        System.out.println("Job Complete.");
    }

    public static void runTracer(String scene) {
        Tracer tracer = new Tracer(800, 800, scene);
        tracer.trace();
        tracer.saveImage();
    }

    public static void runTracer(String scene, String saveFile) {
        Tracer tracer = new Tracer(800, 800, scene);
        tracer.trace();
        tracer.saveImage(saveFile);
    }

    public static void makeGIFImages() {
        for (int i = 0; i < 50; i++) {
            System.out.println("Render Scene: " + i);
            runTracer("movie/test3-" + i + ".scene", "save/" + i + ".png");
        }
    }

}
