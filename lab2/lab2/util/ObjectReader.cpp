//
//  ObjectReader.cpp
//  lab2
//
//  Created by Mitchell Tenney on 9/14/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//


#include <GLUT/GLUT.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "ObjectReader.h"
#include "Logger.h"
#include "dirent.h"


using namespace std;

ObjectReader::ObjectReader() {}

ObjectReader::~ObjectReader() {}

void ObjectReader::load(string filename) {
    ifstream file;
    file.open(filename);
    if(file.is_open()) {
        Logger::debug(filename + " is open");
        
        
        string line;
        while(!file.eof()) {
            getline(file, line);
            stringstream ss(line);
            vector<string> tokens;
            string token;
            while(ss >> token) {
                tokens.push_back(token);
            }
            if(!tokens.empty()) {
                if(tokens[0] == "v") {
                    //                Logger::debug("VERTEX");
                    createVertex(tokens);
                } else if(tokens[0] == "vt") {
                    //                Logger::debug("VERTEX TEXTURE");
                    createVertexTexture(tokens);
                } else if(tokens[0] == "vn") {
                    //                Logger::debug("VERTEX NORMAL");
                    createVertexNormal(tokens);
                } else if(tokens[0] == "f") {
                    //                Logger::debug("FACE");
                    createFace(tokens);
                }
            }
        }
        if(Logger::enableDebug) {
            int temp = (int) vertices.size();
            stringstream ss;
            ss << temp;
            string size = ss.str();
            Logger::debug("Vertices: " + size);
            ss.clear();
            ss.str(string());
            temp = (int) textures.size();
            ss << temp;
            size = ss.str();
            Logger::debug("Textures: " + size);
            ss.clear();
            ss.str(string());
            temp = (int) normals.size();
            ss << temp;
            size = ss.str();
            Logger::debug("Normals: " + size);
            ss.clear();
            ss.str(string());
            temp = (int) faces.size();
            ss << temp;
            size = ss.str();
            Logger::debug("Faces: " + size);
        }
        file.close();
        if(!file.is_open()) {
            Logger::debug(filename + " is closed");
        }
        
    } else {
        Logger::error(filename + " did not open.");
    }
    
}

void ObjectReader::draw() {
    for(int f = 0; f < faces.size(); f++) {
        glBegin(GL_POLYGON);
        for(int v = 0; v < faces[f].vals.size(); v++) {
            int index = faces[f].vals[v].v;
            GeometricVertex vertex = vertices[index - 1];
            TextureVertex texVertex = textures[index - 1];
            glTexCoord2f(texVertex.u, texVertex.v);
            glVertex3f(vertex.x, vertex.y, vertex.z);
        }
        glEnd();
    }
}



