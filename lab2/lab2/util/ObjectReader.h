//
//  ObjectReader.h
//  lab2
//
//  Created by Mitchell Tenney on 9/14/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef __lab2__ObjectReader__
#define __lab2__ObjectReader__

#include <stdio.h>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

struct GeometricVertex {
    float x;
    float y;
    float z;
};

struct TextureVertex {
    float u;
    float v;
};

struct NormalVertex {
    float i;
    float j;
    float k;
};

struct FaceValue {
    int v;
    int vt;
    int vn;
};

struct Face {
    vector<FaceValue> vals;
};


class ObjectReader {
public:
    ObjectReader();
    ~ObjectReader();
    void load(string filename);
    void draw();
    
    vector<GeometricVertex> vertices;
    vector<TextureVertex> textures;
    vector<NormalVertex> normals;
    vector<Face> faces;
    
private:
    
    void createVertex(vector<string> &tokens) {
        GeometricVertex v = GeometricVertex();
        v.x = stof(tokens[1]);
        v.y = stof(tokens[2]);
        v.z = stof(tokens[3]);
        vertices.push_back(v);
    }
    
    void createVertexTexture(vector<string> &tokens) {
        TextureVertex vt = TextureVertex();
        vt.u = stof(tokens[1]);
        vt.v = stof(tokens[2]);
        textures.push_back(vt);
    }
    
    void createVertexNormal(vector<string> &tokens) {
        NormalVertex vn = NormalVertex();
        vn.i = stof(tokens[1]);
        vn.j = stof(tokens[2]);
        vn.k = stof(tokens[3]);
        normals.push_back(vn);
    }
    
    void createFace(vector<string> &tokens) {
        Face face = Face();
        for(int i = 1; i < tokens.size(); i++) {
            vector<string> tokenValues = splitFace(tokens[i]);
            FaceValue faceValue = FaceValue();
            faceValue.v = stoi(tokenValues[0]);
            faceValue.vt = stoi(tokenValues[1]);
            faceValue.vn = stoi(tokenValues[2]);
            face.vals.push_back(faceValue);
        }
        faces.push_back(face);
        
    }
    
    vector<string> splitFace(string &token) {
        vector<string> tokenValues = vector<string>();
        stringstream ss(token);
        string item;
        while(getline(ss, item, '/')) {
            tokenValues.push_back(item);
        }
        return tokenValues;
    }
    
};


#endif /* defined(__lab2__ObjectReader__) */
