//
//  openGL.h
//  lab2
//
//  Created by Mitchell Tenney on 9/14/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef lab2_openGL_h
#define lab2_openGL_h

#ifdef WIN32
#include <windows.h>
#endif

// For all operating systems
#include <OpenGL/GL.h>
#include <OpenGL/GLU.h>
#include <GLUT/glut.h>
#include "SOIL.h"

#include "ObjectReader.h"


#define KEYBOARD_S 115
#define KEYBOARD_ESC 27
#define KEYBOARD_F 102
#define NORMAL_EXIT_GLUT_LOOP "Terminating Lab 2"
bool specialKeys[1000] = {0};

int width = 640;
int height = 480;
int stereo = 0;
int fullscreen = 0;
GLuint textures[1];
float angle = 35;

vector<ObjectReader> objects;

void handleKeyboardInput() {
    if(specialKeys[GLUT_KEY_LEFT]){
        Logger::debug("Left Key Pressed");
    }
    if(specialKeys[GLUT_KEY_RIGHT]){
        Logger::debug("Right Key Pressed");
    }
    if(specialKeys[GLUT_KEY_UP]){
        Logger::debug("Up Key Pressed");
    }
    if(specialKeys[GLUT_KEY_DOWN]) {
        Logger::debug("Down Key Pressed");
    }
}

void drawObjects() {
    
    glColor3f(1.0f, 1.0f, 1.0f);
    for(auto obj : objects) {
        obj.draw();
    }
}


void drawScene() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glViewport(0, 0, width, height);
    
    glTranslatef(0.0f, 0.0f, -30);
    glRotatef(angle, 0, 1, 0);
    
    drawObjects();
    
    glFlush();
    glutSwapBuffers();
    angle += .5;
    if(angle == 360) {
        angle = 0;
    }
}

void idle() {
    handleKeyboardInput();
    drawScene();
}

void resize(int w, int h) {
    if(h == 0)
        h = 1;
    glViewport(0,0,width,height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(stereo)
        gluPerspective(45.0f, (GLfloat)width/((GLfloat)height * 2.0f), 0.1f, 2000.0f);
    else
        gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 2000.0f);
    width = w;
    height = h;
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void keyDown(unsigned char key, int x, int y) {
    if(key == KEYBOARD_ESC)
        throw NORMAL_EXIT_GLUT_LOOP;
    if(key == KEYBOARD_F){
        if(stereo)
            return;
        if(fullscreen){
            Logger::debug("Disable Fullscreen");
            fullscreen = 0;
            glutReshapeWindow(width, height);
            glutPositionWindow(100,100);
        } else {
            Logger::debug("Enable Fullscreen");
            glutFullScreen();
            fullscreen = 1;
        }
    }
}

void specialKeysFunc(int key, int x, int y) {
    switch(key){
        case GLUT_KEY_LEFT:
            specialKeys[GLUT_KEY_LEFT] = 1;
            break;
        case GLUT_KEY_RIGHT:
            specialKeys[GLUT_KEY_RIGHT] = 1;
            break;
        case GLUT_KEY_UP:
            specialKeys[GLUT_KEY_UP] = 1;
            break;
        case GLUT_KEY_DOWN:
            specialKeys[GLUT_KEY_DOWN] = 1;
            break;
        default:
            break;
    }
}

void specialKeysUpFunc(int key, int x, int y) {
    switch(key){
        case GLUT_KEY_LEFT:
            specialKeys[GLUT_KEY_LEFT] = 0;
            break;
        case GLUT_KEY_UP:
            specialKeys[GLUT_KEY_UP] = 0;
            break;
        case GLUT_KEY_RIGHT:
            specialKeys[GLUT_KEY_RIGHT] = 0;
            break;
        case GLUT_KEY_DOWN:
            specialKeys[GLUT_KEY_DOWN] = 0;
            break;
        default:
            break;
    }
}

void loadTextures() {
    Logger::debug("loading textures");
    textures[0] = SOIL_load_OGL_texture("crayon.png",
                                        SOIL_LOAD_AUTO,
                                        SOIL_CREATE_NEW_ID,
                                        SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
                                        );
    
    if( 0 == textures[0] ) {
        Logger::error("SOIL LOADING ERROR");
    }
//    glGenTextures(1, textures);
    
    
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 128, 128, 0, GL_RGB, GL_FLOAT, imageData);

}


void initGL(int argc, char **argv) {
    Logger::debug("init GL");
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(width, height);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Lab 2 - Texturing");
    
    // Important GL initialization commands, can be left alone for most things
    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
//    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_COLOR_MATERIAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    
    // GL Function calls
    glutDisplayFunc(drawScene);
    glutIdleFunc(idle);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keyDown);
    glutSpecialFunc(specialKeysFunc);
    glutSpecialUpFunc(specialKeysUpFunc);
    
    loadTextures();
    
    try {
        glutMainLoop();
    } catch (const char* msg) {
        Logger::info(msg);
    }
}

#endif
