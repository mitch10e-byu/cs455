#include <iostream>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "Logger.h"
#include "openGL.h"
#include "ObjectReader.h"


using namespace std;

void loadObj() {
    const char *filename;
//    ObjectReader test = ObjectReader();
//    filename = "test.obj";
//    test.load(filename);
//    objects.push_back(test);
    
    ObjectReader crayon = ObjectReader();
    filename = "crayon.obj";
    crayon.load(filename);
    objects.push_back(crayon);

    
//    ObjectReader crayonBox = ObjectReader();
//    filename = "crayonBox.obj";
//    crayonBox.load(filename);
//    objects.push_back(crayonBox);
//
//    ObjectReader mole = ObjectReader();
//    filename = "molecart.obj";
//    mole.load(filename);
//    objects.push_back(mole);
    
    
}

void loadMaterials() {
    
}


int main (int argc, char **argv) {
    Logger::enableDebug = true;
    Logger::info("Lab 2 - Texturing");
    Logger::info("Created By: Mitchell Tenney");
    Logger::info("CS 455 - Fall 2015");
    loadObj();
    loadMaterials();
    initGL(argc, argv);
    return 0;
}