//
//  Vector4D.h
//  lab3
//
//  Created by Mitchell Tenney on 10/5/15.
//  Copyright © 2015 Mitchell Tenney. All rights reserved.
//

#ifndef Vector4D_h
#define Vector4D_h
#pragma once

#include <stdio.h>
#include <math.h>
#include <iomanip>
#include <string>

class Vector4D {
public:
    Vector4D();
    Vector4D(float *d);
    Vector4D(float x, float y, float z, float w = 1);
    Vector4D(const Vector4D &copy);
        
    float distance(Vector4D &v);
    float length();
    float dot(Vector4D &v);
    Vector4D cross(Vector4D &v);
    Vector4D normal();
    
    // Operators
    float       &operator[] (int index);
    const float& operator[](int index) const;
    
    Vector4D    operator*  (const float scalar);
    Vector4D    operator/  (const float scalar);
    Vector4D    operator+  (Vector4D &v);
    Vector4D    operator-  (Vector4D &v);
    Vector4D    operator*  (Vector4D &v);
    Vector4D    operator/  (Vector4D &v);
    
    Vector4D    &operator*= (const float scalar);
    Vector4D    &operator/= (const float scalar);
    Vector4D    &operator=  (const Vector4D &v);
    Vector4D    &operator+= (const Vector4D &v);
    Vector4D    &operator-= (const Vector4D &v);
    Vector4D    &operator*= (const Vector4D &v);
    Vector4D    &operator/= (const Vector4D &v);

private:
    static const int size = 4;
    float _data[size];
    
    void normalize() {
        float l = length();
        for (int i = 0; i < size - 1; i++) {
            _data[i] /= l;
        }
    }
};

#endif /* Vector4D_h */
