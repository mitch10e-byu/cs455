//
//  Matrix.cpp
//  lab3
//
//  Created by Mitchell Tenney on 10/5/15.
//  Copyright © 2015 Mitchell Tenney. All rights reserved.
//

#include "Matrix.h"
#include <sstream>

using namespace std;

// https://www.talisman.org/opengl-1.1/Reference/glMultMatrix.html
// OpenGL goes down the columns instead of across the rows

Matrix::Matrix() {
    float d[size] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
    for (int i = 0; i < size; i++) {
        _data[i] = d[i];
    }
}

Matrix::Matrix(float *d) {
    for (int i = 0; i < size; i++) {
        _data[i] = d[i];
    }
}

Matrix::Matrix(const Matrix &copy) {
    for (int i = 0; i < size; i++) {
        _data[i] = copy._data[i];
    }
}

// http://stackoverflow.com/questions/8101489/c-no-matching-constructor-for-initialization-of-compiler-error
Matrix Matrix::Identity() {
    return Matrix();
}

Matrix Matrix::Rotate(char axis, float angle) {
    switch (axis) {
        case 'x':
            return RotateX(angle);
            break;
        case 'y':
            return RotateY(angle);
            break;
        case 'z':
            return RotateZ(angle);
            break;
        default:
            return Identity();
            break;
    }
}

Matrix Matrix::RotateAll(float x, float y, float z) {
    Matrix result = Identity();
    result *= RotateX(x);
    result *= RotateY(y);
    result *= RotateZ(z);
    return result;
}

Matrix Matrix::Translate(float x, float y, float z) {
    float d[size] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        x, y, z, 1
    };
    
    return Matrix(d);
}

Matrix Matrix::Scale(float x, float y, float z) {
    float d[size] = {
        x, 0, 0, 0,
        0, y, 0, 0,
        0, 0, z, 0,
        0, 0, 0, 1
    };
    return Matrix(d);
}

float Matrix::at(int row, int col) {
    return _data[row + col * 4];
}

float* Matrix::matrix() {
    return _data;
}

string Matrix::info() {
    stringstream ss;
    ss << _data[0] << " " << _data[1] << " " << _data[2] << " " << _data[3] << endl;
    ss << _data[4] << " " << _data[5] << " " << _data[6] << " " << _data[7] << endl;
    ss << _data[8] << " " << _data[9] << " " << _data[10] << " " << _data[11] << endl;
    ss << _data[12] << " " << _data[13] << " " << _data[14] << " " << _data[15] << endl;
    return ss.str();
}

// Operators

float& Matrix::operator[] (int index) {
    return _data[index];
}

const float& Matrix::operator[] (int index) const{
    return _data[index]; //const
}

Vector4D Matrix::operator* (Vector4D &v) {
    float d[4];
    for (int i = 0; i < 4; i++) {
        d[i] =
            _data[0 + i] * v[0] +
            _data[4 + i] * v[1] +
            _data[8 + i] * v[2] +
            _data[12+ i] * v[3];
    }
    return Vector4D(d);
}

Matrix Matrix::operator*  (const float scalar) {
    Matrix result = *this;
    result *= scalar;
    return result;
}

Matrix Matrix::operator+  (Matrix &m) {
    Matrix result = *this;
    result += m;
    return result;
}

Matrix Matrix::operator*  (Matrix &m) {
    Matrix result = *this;
    result *= m;
    return result;
}

Matrix& Matrix::operator*= (const float scalar) {
    for (int i = 0; i < size; i++) {
        _data[i] *= scalar;
    }
    return *this;
}

Matrix& Matrix::operator=  (const Matrix &m) {
    for (int i = 0; i < size; i++) {
        _data[i] = m[i];
    }
    return *this;
}

Matrix& Matrix::operator+= (const Matrix &m) {
    for (int i = 0; i < size; i++) {
        _data[i] += m[i];
    }
    return *this;
}

Matrix& Matrix::operator*= (const Matrix &m) {
    float d[size];
    for (int i = 0; i < size; i++) {
        int row = i % 4;
        int col = i - row;
        float total = 0;
        total += _data[0 + row] * m[0 + col];
        total += _data[4 + row] * m[1 + col];
        total += _data[8 + row] * m[2 + col];
        total += _data[12+ row] * m[3 + col];
        d[i] = total;
    }
    for (int i = 0; i < size; i++) {
        _data[i] = d[i];
    }
    return *this;
}




