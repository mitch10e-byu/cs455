//
//  Logger.h
//  lab2
//
//  Created by Mitchell Tenney on 9/4/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef __lab2__Logger__
#define __lab2__Logger__

#include <stdio.h>
#include <iostream>
#include <string>
using namespace std;

#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */

#define ARRAY_SIZE(array) (sizeof((array))/sizeof((array[0])))


class Logger {
public:
    static bool enableDebug;
    static void print(string message);
    static void debug(string message);
    static void info(string message);
    static void warn(string message);
    static void error(string message);
private:
    static void log(string message, const char *COLOR);
};


#endif /* defined(__lab2__Logger__) */
