//
//  Logger.cpp
//  lab2
//
//  Created by Mitchell Tenney on 9/4/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//


#include "Logger.h"

bool Logger::enableDebug = false;

void Logger::print(string message) {
    cout << message << endl;
}

void Logger::log(string message, const char *COLOR) {
    cout << COLOR << message << RESET << endl;
}

void Logger::debug(string message) {
    if(Logger::enableDebug) {
        message = "[DEBUG]\t\t" + message;
        log(message, GREEN);
    }
}

void Logger::info(string message) {
    message = "[INFO ]\t\t" + message;
    log(message, CYAN);
}

void Logger::warn(string message) {
    message = "[WARN ]\t\t" + message;
    log(message, YELLOW);
}

void Logger::error(string message) {
    message = "[ERROR]\t\t" + message;
    log(message, RED);
}
