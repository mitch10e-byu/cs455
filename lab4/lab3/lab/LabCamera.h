//
//  LabCamera.h
//  lab3
//
//  Created by Mitchell Tenney on 10/5/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef __lab3__LabCamera__
#define __lab3__LabCamera__

#include <stdio.h>
#include "Logger.h"
#include "LabObjectElements.h"

class LabCamera {
public:
    static LabCamera* get();
    void home();
    vector3D position();
    vector3D rotation();
    
    void moveForward(float speed);
    void moveBackward(float speed);
    
    void strafeLeft(float speed);
    void strafeRight(float speed);
    
    void moveUp();
    void moveDown();
    
    void turnLeft(float speed);
    void turnRight(float speed);
    
    void lookDown(float speed);
    void lookUp(float speed);
    
private:
    LabCamera();
    static LabCamera *_instance;
    vector3D _position;
    vector3D _rotation;
    const float _speed = 0.15;
    const float _rotationSpeed = 10;
    const Vector4D _home = Vector4D(-10.5, -2, -6);

};

#endif /* defined(__lab3__LabCamera__) */
