//
//  LabObjectLoader.hpp
//  lab3
//
//  Created by Mitchell Tenney on 10/2/15.
//  Copyright © 2015 Mitchell Tenney. All rights reserved.
//

#ifndef LabObjectLoader_hpp
#define LabObjectLoader_hpp
#pragma once

#include <stdio.h>
#include <sstream>
#include "LabObject.h"
#include "LabObjectElements.h"

using namespace std;

class LabObjectLoader {
public:
    LabObject loadObject(string objectName);
    
private:
    string resourceFolder = "resource/";
    
    vector3D createVector3D(vector<string> &tokens) {
        vector3D v;
        v.x = stof(tokens[1]);
        v.y = stof(tokens[2]);
        v.z = stof(tokens[3]);
        return v;
    }
    
    vector2D createVector2D(vector<string> &tokens) {
        vector2D vt;
        vt.x = stof(tokens[1]);
        vt.y = stof(tokens[2]);
        return vt;
    }
    
    face_t createFace(vector<string> &tokens) {
        face_t f;
        for(int i = 1; i < tokens.size(); i++) {
            vector<string> tokenValues = splitFace(tokens[i]);
            face_value value;
            value.vertex = stoi(tokenValues[0]);
            value.texture = stoi(tokenValues[1]);
            value.normal = stoi(tokenValues[2]);
            f.values.push_back(value);
        }
        return f;
    }
    
    vector<string> splitFace(string &token) {
        vector<string> tokenValues = vector<string>();
        stringstream ss(token);
        string item;
        while(getline(ss, item, '/')) {
            tokenValues.push_back(item);
        }
        return tokenValues;
    }
    
};

#endif /* LabObjectLoader_hpp */
