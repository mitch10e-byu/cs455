//
//  LabObject.cpp
//  lab3
//
//  Created by Mitchell Tenney on 9/17/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#include <GLUT/GLUT.h>

#include <sstream>
#include "LabObject.h"

LabObject::LabObject() :
_rotation(      Matrix::Identity()),
_scale(         Matrix::Identity()),
_translation(   Matrix::Identity()) {
    _parent = NULL;
    yRotation = 0;
}

string LabObject::getID() {
    return _id;
}

void LabObject::setID(string ID) {
    _id = ID;
}

mesh_t LabObject::getMesh() {
    return _mesh;
}

void LabObject::setMesh(mesh_t mesh) {
    _mesh = mesh;
}

LabObject* LabObject::getParent() {
    return _parent;
}

void LabObject::setParent(LabObject *parent) {
    _parent = parent;
}

void LabObject::rotatev(vector3D &v) {
    _rotation *= Matrix::RotateAll(v.x, v.y, v.z);
}

void LabObject::rotatef(float x, float y, float z) {
    _rotation *= Matrix::RotateAll(x, y, z);
}

void LabObject::scalev(vector3D &v) {
    _scale *= Matrix::Scale(v.x, v.y, v.z);
}

void LabObject::scalef(float x, float y, float z) {
    _scale *= Matrix::Scale(x, y, z);
}

void LabObject::translatev(vector3D &v) {
    _translation *= Matrix::Translate(v.x, v.y, v.z);
}

void LabObject::translatef(float x, float y, float z) {
    _translation *= Matrix::Translate(x, y, z);
}

void LabObject::transform() {
    glPushMatrix();
    if(_parent != NULL) {
        _parent->transform();
    }
    glMultMatrixf(_translation.matrix());
    glMultMatrixf(_scale.matrix());
    glMultMatrixf(_rotation.matrix());
}

void LabObject::draw() {
    if(_id != "mole") {
        glBegin(GL_TRIANGLES);
    } else {
        glBegin(GL_POLYGON);
    }
    for(int f = 0; f < _mesh.faces.size(); f++) {
        face_t face = _mesh.faces[f];
        for(int i = 0; i < face.values.size(); i++) {
            int v = face.values[i].vertex;
            int t = face.values[i].texture;
            int n = face.values[i].normal;
            
            vector3D vertex = _mesh.vertex[v - 1];
            vector2D texVertex = _mesh.uv[t - 1];
            vector3D normal = _mesh.normal[n - 1];
            glNormal3f(normal.x, normal.y, normal.z);
            glTexCoord2f(texVertex.x, texVertex.y);
            glVertex3f(vertex.x, vertex.y, vertex.z);
            
        }
    }
    glEnd();
    glPopMatrix();
}


void LabObject::getInfo() {
    if(Logger::enableDebug) {
        Logger::debug(_id);
        int temp = (int) _mesh.vertex.size();
        stringstream ss;
        ss << temp;
        string size = ss.str();
        Logger::debug("Vertices: " + size);
        ss.clear();
        ss.str(string());
        temp = (int) _mesh.uv.size();
        ss << temp;
        size = ss.str();
        Logger::debug("Textures: " + size);
        ss.clear();
        ss.str(string());
        temp = (int) _mesh.normal.size();
        ss << temp;
        size = ss.str();
        Logger::debug("Normals: " + size);
        ss.clear();
        ss.str(string());
        temp = (int) _mesh.faces.size();
        ss << temp;
        size = ss.str();
        Logger::debug("Faces: " + size);
    }
}