//
//  LabObjectReader.cpp
//  lab3
//
//  Created by Mitchell Tenney on 9/17/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#include "LabObjectReader.h"
#include <fstream>

LabObjectReader::LabObjectReader() {}

LabObjectReader::~LabObjectReader() {}

bool LabObjectReader::load(string obj, string tex) {
    bool didLoad = true;
    ifstream in(obj);
    if(!in) {
        didLoad = false;
        Logger::error(".obj file cannot be opened");
    }
    string line;
    while(getline(in, line)) {
        vector<string> tokens = getTokens(line);
        for(auto token : tokens) {
            Logger::debug(token);
        }
    }
    
    
    
    return didLoad;
}
