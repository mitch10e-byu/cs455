//
//  LabControls.h
//  lab3
//
//  Created by Mitchell Tenney on 9/18/15.
//  Copyright (c) 2015 Mitchell Tenney. All rights reserved.
//

#ifndef lab3_LabControls_h
#define lab3_LabControls_h
#pragma once

//Only for windows
#ifdef WIN32
#include <Windows.h>
#include <MMSystem.h>
#endif // WIN32

// For all operating systems
#include <OpenGL/GL.h>
#include <OpenGL/GLU.h>
#include <GLUT/glut.h>

int cameraSpeed = 0.5;

void fullscreen() {
    glutSetCursor(GLUT_CURSOR_NONE); 
    glutFullScreen();
}

void window(int w, int h) {
    glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
    glutPositionWindow((glutGet(GLUT_SCREEN_WIDTH) - w) / 2,
                       (glutGet(GLUT_SCREEN_HEIGHT) - h) / 2);
    glutReshapeWindow(w, h);
}

#endif
